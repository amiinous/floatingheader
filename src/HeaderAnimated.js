/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  Animated,
  Image,
  ScrollView,
  View,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import {TabView, SceneMap} from 'react-native-tab-view';

const SCREEN_WIDTH = Dimensions.get('screen').width;

import img from './unnamed.jpeg';
import arrow from './arrow.png';

const DATA = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1];

export class HeaderAnimated extends Component {
  constructor() {
    super();
    this.animValue = new Animated.ValueXY();
    this.profBox = Animated.subtract(390, this.animValue);

    this.state = {index: 0};
  }
  renderHeader = () => {
    return <Image source={img} />;
  };

  FirstRoute = () => <View style={{flex: 1, backgroundColor: '#ff4081'}} />;

  SecondRoute = () => <View style={{flex: 1, backgroundColor: '#673ab7'}} />;

  _onLayout = ({
    nativeEvent: {
      layout: {x, y, width, height},
    },
  }) => {
    console.log('pos  ', height);
  };

  renderScene = SceneMap({
    first: this.FirstRoute,
    second: this.SecondRoute,
  });

  renderItem = () => {
    return (
      <View
        style={{
          backgroundColor: '#80bf60',
          height: 100,
          borderColor: '#000',
          borderWidth: 1,
        }}
      />
    );
  };
  render() {
    const {index} = this.state;
    return (
      <TabView
        navigationState={{
          index,
          routes: [
            {key: 'first', title: 'First'},
            {key: 'second', title: 'Second'},
          ],
        }}
        renderScene={this.renderScene}
        onIndexChange={(index) => this.setState({index})}
        initialLayout={{width: SCREEN_WIDTH}}
      />
    );
    return (
      <View>
        <ScrollView
          data={DATA}
          renderItem={this.renderItem}
          scrollEventThrottle={1}
          //   style={{backgroundColor: 'yellow'}}
          //   onScroll={(event) =>
          //     console.log('charr  ', 390 - event.nativeEvent.contentOffset.y)
          //   }
          onScroll={Animated.event(
            [{nativeEvent: {contentOffset: {y: this.animValue.y}}}],
            {useNativeDriver: false},
          )}>
          <Animated.View>
            <Animated.Image
              resizeMode="cover"
              onLayout={this._onLayout}
              style={{
                backgroundColor: '#000',
                width: SCREEN_WIDTH,
                height: SCREEN_WIDTH,
                transform: [
                  {
                    translateY: this.animValue.y.interpolate({
                      inputRange: [-200, 0],
                      outputRange: [-150, 0],
                      extrapolate: 'clamp',
                    }),
                  },
                  {
                    scale: this.animValue.y.interpolate({
                      inputRange: [-200, 0],
                      outputRange: [2, 1],
                      extrapolate: 'clamp',
                    }),
                  },
                ],
              }}
              source={img}
            />
          </Animated.View>

          {DATA.map(() => (
            <View
              style={{
                backgroundColor: '#80bf60',
                height: 100,
                borderColor: '#000',
                borderWidth: 1,
              }}
            />
          ))}
        </ScrollView>
        <Animated.View
          style={{
            position: 'absolute',
            top: 0,
            left: 0,
            height: SCREEN_WIDTH,
            width: SCREEN_WIDTH,
            transform: [
              {
                translateY: this.animValue.y.interpolate({
                  inputRange: [-300, 0, SCREEN_WIDTH - 64, 1000],
                  outputRange: [300, 0, 64 - SCREEN_WIDTH, 64 - SCREEN_WIDTH],
                  extrapolate: 'clamp',
                }),
              },
            ],
            backgroundColor: this.animValue.y.interpolate({
              inputRange: [0, 250],
              outputRange: ['rgba(0, 51, 102, 0)', 'rgba(0,51,102,1)'],
              extrapolate: 'clamp',
            }),
            // opacity: this.animValue.y.interpolate({
            //   inputRange: [200, 400],
            //   outputRange: [0, 1],
            //   extrapolate: 'clamp',
            // }),
          }}>
          <Animated.Text
            style={{
              position: 'absolute',
              bottom: 34,
              left: 12,
              color: '#FFF',
              fontSize: 30,
              fontWeight: 'bold',
              transform: [
                {
                  translateX: this.animValue.y.interpolate({
                    inputRange: [100, SCREEN_WIDTH - 100],
                    outputRange: [0, 50],
                    extrapolate: 'clamp',
                  }),
                },
              ],
            }}>
            Digikala Company
          </Animated.Text>
          <Animated.Text
            style={{
              position: 'absolute',
              bottom: 10,
              left: 12,
              color: '#FFF',
              fontSize: 20,
              transform: [
                {
                  translateX: this.animValue.y.interpolate({
                    inputRange: [100, SCREEN_WIDTH - 100],
                    outputRange: [0, 50],
                    extrapolate: 'clamp',
                  }),
                },
              ],
            }}>
            Last Seen at 23:23
          </Animated.Text>
        </Animated.View>
        <TouchableOpacity
          style={{
            position: 'absolute',
            top: 12,
            left: 12,
          }}>
          <Image
            source={arrow}
            style={{height: 32, width: 32, tintColor: '#FFF'}}
          />
        </TouchableOpacity>
      </View>
    );
  }
}

export default HeaderAnimated;
